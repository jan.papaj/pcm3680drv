#pragma once

typedef struct{
	USHORT       port;
	UCHAR        ff;
	UCHAR        rtr;
	ULONG        id;
	UCHAR        dlen;
	UCHAR        data[8];
	UCHAR        a1;
	UCHAR        a2;
	UCHAR        a3;
} CanMsg_t;
