/*++

Copyright (c) 1990-2000 Microsoft Corporation, All Rights Reserved

Module Name:

    gpioctl.h

Abstract:

    Include file for Generic Port I/O Example Driver

--*/

#if     !defined(__GPIOCTL_H__)
#define __GPIOCTL_H__

#define DRV_SUCCESS 0
#define DRV_ERROR   1


//
// Define the IOCTL codes we will use.  The IOCTL code contains a command
// identifier, plus other information about the device, the type of access
// with which the file must have been opened, and the type of buffering.
//

//
// Device type           -- in the "User Defined" range."
//

#define PCM3680_TYPE 40000

// The IOCTL function codes from 0x800 to 0xFFF are for customer use.

#define IOCTL_CAN_OPEN\
    CTL_CODE( PCM3680_TYPE, 0x900, METHOD_BUFFERED, FILE_READ_ACCESS)

#define IOCTL_CAN_CLOSE \
    CTL_CODE( PCM3680_TYPE, 0x901, METHOD_BUFFERED, FILE_READ_ACCESS)

#define IOCTL_CAN_INIT \
    CTL_CODE( PCM3680_TYPE, 0x902, METHOD_BUFFERED, FILE_READ_ACCESS)

#define IOCTL_CAN_RESET \
    CTL_CODE(PCM3680_TYPE,  0x903, METHOD_BUFFERED, FILE_READ_ACCESS)

#define IOCTL_CAN_SET_OUT_CTRL \
    CTL_CODE(PCM3680_TYPE,  0x904, METHOD_BUFFERED, FILE_READ_ACCESS)

#define IOCTL_CAN_SET_ACP \
    CTL_CODE(PCM3680_TYPE,  0x905, METHOD_BUFFERED, FILE_READ_ACCESS)

#define IOCTL_CAN_SET_BAUD \
    CTL_CODE(PCM3680_TYPE,  0x906, METHOD_BUFFERED, FILE_READ_ACCESS)

#define IOCTL_CAN_SET_NORMAL \
    CTL_CODE(PCM3680_TYPE,  0x907, METHOD_BUFFERED, FILE_READ_ACCESS)

#define IOCTL_CAN_ENABLE_RX_INT \
    CTL_CODE(PCM3680_TYPE,  0x908, METHOD_BUFFERED, FILE_READ_ACCESS)

#define IOCTL_CAN_SEND_MSG \
    CTL_CODE(PCM3680_TYPE,  0x909, METHOD_BUFFERED, FILE_READ_ACCESS)

#define IOCTL_CAN_RECEIVE_MSG \
    CTL_CODE(PCM3680_TYPE,  0x920, METHOD_BUFFERED, FILE_READ_ACCESS)

typedef struct _GENPORT_IN
{
  union
  {
    struct _InOpen_t
    {
      ULONG devNum;
    } InOpen;

    struct _InClose_t
    {
      ULONG port;
    } InClose;

    struct _InInit_t
    {
      ULONG port;
      ULONG btr0;
      ULONG btr1;
      ULONG intMask;
    } InInit;

    struct _InReset_t
    {
      ULONG port;
    } InReset;

    struct _InSetOutCtrl_t
    {
      ULONG port;
      ULONG outCtrlCode;
    } InSetOutCtrl;

    struct _InSetAcp_t
    {
      ULONG port;
      ULONG acpCode;
      ULONG acpMask;
    } InSetAcp;

    struct _InSetBaud_t
    {
      ULONG port;
      ULONG btr0;
      ULONG btr1;
    } InSetBaud;

    struct _InSetNormal_t
    {
      ULONG port;
    } InSetNormal;

    struct _InEnableRxInt_t
    {
      ULONG port;
    } InEnableRxInt;
  } u;
} GENPORT_IN, *PGENPORT_IN;

typedef struct _GENPORT_OUT
{
  ULONG state;
  union
  {
    struct _OutOpen_t
    {
      ULONG port;
      ULONG hostId;
      ULONG preBaud;
    } OutOpen;
  } u;
} GENPORT_OUT, *PGENPORT_OUT;

#endif
