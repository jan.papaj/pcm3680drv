/*++

Copyright (c) 1990-2000  Microsoft Corporation

Module Name:

queue.c

Abstract:

This is a C version of a very simple sample driver that illustrates
how to use the driver framework and demonstrates best practices.

--*/

#include "driver.h"

#ifdef ALLOC_PRAGMA
#pragma alloc_text (PAGE, PortIOQueueInitialize)
#pragma alloc_text (PAGE, PortIOEvtIoDeviceControl)
#pragma alloc_text (PAGE, FmEvtIoRead)
#pragma alloc_text (PAGE, FmEvtIoWrite)
#endif

NTSTATUS
PortIOQueueInitialize(
_In_ WDFDEVICE Device
)
{
  WDFQUEUE queue;
  NTSTATUS status;
  WDF_IO_QUEUE_CONFIG    queueConfig;

  PAGED_CODE();

  //
  // Configure a default queue so that requests that are not
  // configure-fowarded using WdfDeviceConfigureRequestDispatching to goto
  // other queues get dispatched here.
  //
  WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(
    &queueConfig,
    WdfIoQueueDispatchSequential
    );

  // 
  // By registering the IOCTL handler for the default queue,
  // the default queue will fail all other requests
  //
  queueConfig.EvtIoDeviceControl = PortIOEvtIoDeviceControl;
  queueConfig.EvtIoRead = FmEvtIoRead;
  queueConfig.EvtIoWrite = FmEvtIoWrite;

  //
  // NOTE: If you need to have the queue's callbacks synchronized 
  // with one another then you should set the WDF_SYNCHRONIZATION_SCOPE
  // enum (member of WDF_OBJECT_ATTRIBUTES) to WdfSynchronizationScopeQueue.
  // When that is done ONE of the following should also be done: 
  // 1) Remove the pragmas that cause the queue's callback functions to be paged
  //    out. Also remove the PAGED_CODE macros from the functions.
  // 2) If you would like to have the callbacks pageable, then set
  //    the WDF_EXECUTION_LEVEL enum (member of WDF_OBJECT_ATTRIBUTES)
  //    to WdfExecutionLevelPassive.
  // 

  //
  // By default, Static Driver Verifier (SDV) displays a warning if it 
  // doesn't find the EvtIoStop callback on a power-managed queue. 
  // The 'assume' below causes SDV to suppress this warning. If the driver 
  // has not explicitly set PowerManaged to WdfFalse, the framework creates
  // power-managed queues when the device is not a filter driver.  Normally 
  // the EvtIoStop is required for power-managed queues, but for this driver
  // it is not needed b/c the driver doesn't hold on to the requests or 
  // forward them to other drivers. This driver completes the requests 
  // directly in the queue's handlers. If the EvtIoStop callback is not 
  // implemented, the framework waits for all driver-owned requests to be
  // done before moving in the Dx/sleep states or before removing the 
  // device, which is the correct behavior for this type of driver.
  // If the requests were taking an indeterminate amount of time to complete,
  // or if the driver forwarded the requests to a lower driver/another stack,
  // the queue should have an EvtIoStop/EvtIoResume.
  //
  __analysis_assume(queueConfig.EvtIoStop != 0);
  status = WdfIoQueueCreate(
    Device,
    &queueConfig,
    WDF_NO_OBJECT_ATTRIBUTES,
    &queue
    );
  __analysis_assume(queueConfig.EvtIoStop == 0);

  if (!NT_SUCCESS(status)) {
    KdPrint(("WdfIoQueueCreate failed 0x%x\n", status));
    return status;
  }

  KdPrint(("PortIOQueueInitialize done\n"));

  return status;
}

VOID
PortIOEvtIoDeviceControl(
_In_ WDFQUEUE     Queue,
_In_ WDFREQUEST   Request,
_In_ size_t       OutputBufferLength,
_In_ size_t       InputBufferLength,
_In_ ULONG        IoControlCode
)
{
  UNREFERENCED_PARAMETER(OutputBufferLength);
  UNREFERENCED_PARAMETER(InputBufferLength);

  PDEVICE_CONTEXT devContext = NULL;
  WDFDEVICE device;
  NTSTATUS status;
  ULONG minDataBufferSize;

  PGENPORT_IN pIn;
  PGENPORT_OUT pOut;

  PAGED_CODE();

  //KdPrint(("PortIOEvtIoDeviceControl\n"));

  device = WdfIoQueueGetDevice(Queue);

  devContext = PortIOGetDeviceContext(device);

  minDataBufferSize = sizeof(GENPORT_OUT);

  status = WdfRequestRetrieveInputBuffer(
    Request,
    sizeof(GENPORT_IN),
    &((PVOID)pIn),
    NULL);

  if (!NT_SUCCESS(status))
  {
    KdPrint(("WdfRequestRetrieveInputBuffer failed!\n"));
  }

  status = WdfRequestRetrieveOutputBuffer(
    Request,
    minDataBufferSize,
    &((PVOID)pOut),
    NULL);

  if (!NT_SUCCESS(status))
  {
    KdPrint(("WdfRequestRetrieveInputBuffer failed!\n"));
  }

  switch (IoControlCode)
  {
  case IOCTL_CAN_OPEN:
    pOut->state = CanOpen(device,
      pIn->u.InOpen.devNum,
      &pOut->u.OutOpen.port,
      &pOut->u.OutOpen.hostId,
      &pOut->u.OutOpen.preBaud);
    break;

  case IOCTL_CAN_CLOSE:
    pOut->state = CanClose(device,
      pIn->u.InClose.port);
    break;

  case IOCTL_CAN_INIT:
    pOut->state = CanInit(device,
      pIn->u.InInit.port,
      pIn->u.InInit.btr0,
      pIn->u.InInit.btr1,
      pIn->u.InInit.intMask);
    break;

  case IOCTL_CAN_RESET:
    pOut->state = CanReset(device,
      pIn->u.InReset.port);
    break;

  case IOCTL_CAN_SET_OUT_CTRL:
    pOut->state = CanSetOutCtrl(device,
      pIn->u.InSetOutCtrl.port,
      pIn->u.InSetOutCtrl.outCtrlCode);
    break;

  case IOCTL_CAN_SET_ACP:
    pOut->state = CanSetAcp(device,
      pIn->u.InSetAcp.port,
      pIn->u.InSetAcp.acpCode,
      pIn->u.InSetAcp.acpMask);

    break;

  case IOCTL_CAN_SET_BAUD:
    pOut->state = CanSetBaud(device,
      pIn->u.InSetBaud.port,
      pIn->u.InSetBaud.btr0,
      pIn->u.InSetBaud.btr1);
    break;

  case IOCTL_CAN_SET_NORMAL:
    pOut->state = CanSetNormal(device,
      pIn->u.InSetNormal.port);
    break;

  case IOCTL_CAN_ENABLE_RX_INT:
    pOut->state = CanEnableRxInt(device,
      pIn->u.InEnableRxInt.port);
    break;

  default:
    KdPrint(("STATUS_INVALID_PARAMETER 0x%08x\n", IoControlCode));
    status = STATUS_INVALID_PARAMETER;
    goto exit;
    break;
  }

  WdfRequestCompleteWithInformation(Request, status, minDataBufferSize);

  return;

exit:
  WdfRequestComplete(Request, status);
}

VOID
FmEvtIoRead(
IN WDFQUEUE     Queue,
IN WDFREQUEST   Request,
IN size_t       Length
)
{
  WDFDEVICE       device;
  NTSTATUS        status;
  CanMsg_t*       pMsg;
  size_t          bufLen;
  ULONG           numReturned;
  ULONG           result;
  ULONG           totalMessages;
  ULONG           ret;

  device = WdfIoQueueGetDevice(Queue);

  totalMessages = 0;
  numReturned = 0;
  ret = 0;
  
  //KdPrint(("FmEvtIoRead\n"));
  status = WdfRequestRetrieveOutputBuffer(Request, Length, &((PVOID)pMsg), &bufLen);
  if (!NT_SUCCESS(status))
  {
    WdfRequestComplete(Request, status);
    return;
  }

  //KdPrint(("FmEvtIoRead buflen: %d\n", bufLen));

  // Valid buffer
  if (bufLen % sizeof(CanMsg_t) == 0)
  {
    // Receive messages from port 0
    result = CanReceiveMsg(device, PORT0, bufLen / sizeof(CanMsg_t), &pMsg[0], &numReturned);
    if (DRV_SUCCESS == result)
    {
      //KdPrint(("FmEvtIoRead port 0 numReturned:%d\n", numReturned));
      totalMessages = numReturned;
    }
    // Receive messages from port 1
    result = CanReceiveMsg(device, PORT1, bufLen / sizeof(CanMsg_t) - totalMessages, &pMsg[totalMessages], &numReturned);
    if (DRV_SUCCESS == result)
    {
      //KdPrint(("FmEvtIoRead port 1 numReturned:%d\n", numReturned));
      totalMessages += numReturned;
    }

    ret = totalMessages * sizeof(CanMsg_t);
  }
  else
  {
    //KdPrint(("FmEvtIoRead: Wrong bufLen%d\n", bufLen));
  }
  
  //KdPrint(("FmEvtIoRead ret: %d\n", ret));

  WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, ret);
}

VOID
FmEvtIoWrite(
IN WDFQUEUE     Queue,
IN WDFREQUEST   Request,
IN size_t        Length
)
{
  WDFDEVICE       device;
  CanMsg_t*       pMsg;
  NTSTATUS        status;
  size_t          length;
  ULONG           result;
  ULONG           ret;

  device = WdfIoQueueGetDevice(Queue);

  ret = 0;

  status = WdfRequestRetrieveInputBuffer(Request, Length, &((PVOID)pMsg), &length);
  if (!NT_SUCCESS(status))
  {
    WdfRequestComplete(Request, status);
    return;
  }
  
  if (length % sizeof(CanMsg_t) == 0)
  {
    result = CanSendMsg(device, pMsg->port, pMsg);
    if (DRV_SUCCESS == result)
    {
      ret = sizeof(CanMsg_t);
    }
  }

  WdfRequestCompleteWithInformation(Request, STATUS_SUCCESS, length);
}
