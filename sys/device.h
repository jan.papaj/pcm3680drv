/*++

Copyright (c) 1990-2000  Microsoft Corporation

Module Name:

    device.h

Abstract:

    This is a C version of a very simple sample driver that illustrates
    how to use the driver framework and demonstrates best practices.

--*/
#include "canbuf.h"
#include "sja1000.h"

#define PORT_COUNT    2

#define PORT0         0
#define PORT1         1

#define PORT0_OFFSET  0x0
#define PORT1_OFFSET  0x200

//
// The device context performs the same job as
// a WDM device extension in the driver frameworks
//
typedef struct _DEVICE_CONTEXT
{
  PVOID PortBase;       // base port address
  ULONG PortCount;      // Count of I/O addresses used.
  ULONG PortMemoryType;
  BOOLEAN PortWasMapped;  // If TRUE, we have to unmap on unload

  WDFINTERRUPT WdfInterrupt[PORT_COUNT];
  ULONG Vector[PORT_COUNT];
  ULONG Irql[PORT_COUNT];
  KAFFINITY Affinity[PORT_COUNT];
  KINTERRUPT_MODE InterruptMode[PORT_COUNT];

  BOOLEAN DevOpened[PORT_COUNT];

} DEVICE_CONTEXT, *PDEVICE_CONTEXT;

//
// This is the Interrupt context for the Serial device. This structure is used
// for keeping track of whether the Interrupt is connected or not.
//
typedef struct _DEVICE_INTERRUPT_CONTEXT {

	//
	// This boolean value indicates whether Interrupt is connected.
	//
	BOOLEAN IsInterruptConnected;

	//
	// This lock is used to synchronize the file close logic and
	// the Surprise Removal logic. When a surprise remove happens,
	// the device interrupts are disabled. When this occurs, the
	// file close logic should not attempt to use the interrupt
	// object.
	//
	WDFWAITLOCK InterruptStateLock;

} DEVICE_INTERRUPT_CONTEXT, *PDEVICE_INTERRUPT_CONTEXT;

WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(DEVICE_INTERRUPT_CONTEXT,
	DeviceGetInterruptContext)

//
// Function to initialize the device and its callbacks
//
NTSTATUS
PortIODeviceCreate(
    PWDFDEVICE_INIT DeviceInit
    );

//
// Device events
//
EVT_WDF_DEVICE_PREPARE_HARDWARE PortIOEvtDevicePrepareHardware;
EVT_WDF_DEVICE_RELEASE_HARDWARE PortIOEvtDeviceReleaseHardware;

EVT_WDF_INTERRUPT_ENABLE PortIOEvtInterruptEnable;
EVT_WDF_INTERRUPT_DISABLE PortIOEvtInterruptDisable;

EVT_WDF_INTERRUPT_ISR PortIOISR0;
EVT_WDF_INTERRUPT_ISR PortIOISR1;

VOID
DeviceSetInterruptPolicy(
_In_ WDFINTERRUPT WdfInterrupt
);

ULONG CanOpen(WDFDEVICE device, ULONG devNum, ULONG *pPort, ULONG *pHostId, ULONG *pBaud);
ULONG CanClose(WDFDEVICE device, ULONG port);
ULONG CanInit(WDFDEVICE device, ULONG port, ULONG btr0, ULONG btr1, ULONG intMask);
ULONG CanReset(WDFDEVICE device, ULONG port);
ULONG CanSetOutCtrl(WDFDEVICE device, ULONG port, ULONG outCtrlCode);
ULONG CanSetAcp(WDFDEVICE device, ULONG port, ULONG acpCode, ULONG acpMask);
ULONG CanSetBaud(WDFDEVICE device, ULONG port, ULONG btr0, ULONG btr1);
ULONG CanSetNormal(WDFDEVICE device, ULONG port);
ULONG CanEnableRxInt(WDFDEVICE device, ULONG port);
ULONG CanSendMsg(WDFDEVICE device, ULONG port, CanMsg_t *pMsg);
ULONG CanReceiveMsg(WDFDEVICE device, ULONG port, ULONG maxCount, CanMsg_t *pMsg, ULONG *pNumReturned);
