/*++

Copyright (c) Microsoft

Module Name:

    device.c - Device handling events for example driver.

Abstract:

    This is a C version of a very simple sample driver that illustrates
    how to use the driver framework and demonstrates best practices.

--*/


#include "driver.h"

#ifdef ALLOC_PRAGMA
#pragma alloc_text (PAGE, PortIODeviceCreate)
#pragma alloc_text (PAGE, PortIOEvtDevicePrepareHardware)
#pragma alloc_text (PAGE, PortIOEvtDeviceReleaseHardware)
#endif

static CanBuf_t canFifoRx[PORT_COUNT];
static CanBuf_t canFifoTx[PORT_COUNT];
static BOOLEAN portSending[PORT_COUNT];

static UCHAR _CanRegRead(ULONG_PTR pMemBase, ULONG offset, ULONG port);
static VOID _CanRegWrite(ULONG_PTR pMemBase, ULONG offset, ULONG port, UCHAR val);
static VOID _Send(ULONG_PTR pMemBase, ULONG port, CanMsg_t *pMsg);
static VOID _IntHandler(WDFDEVICE device, ULONG port);

static UCHAR _CanRegRead(ULONG_PTR pMemBase, ULONG offset, ULONG port)
{
	UCHAR val;

	val = 0;

	if (PORT0 == port)
	{
		val = READ_REGISTER_UCHAR((PUCHAR)(pMemBase + PORT0_OFFSET + offset));
	}
	else if (PORT1 == port)
	{
		val = READ_REGISTER_UCHAR((PUCHAR)(pMemBase + PORT1_OFFSET + offset));
	}

	return val;
}


static VOID _CanRegWrite(ULONG_PTR pMemBase, ULONG offset, ULONG port, UCHAR val)
{
	if (PORT0 == port)
	{
    WRITE_REGISTER_UCHAR((PUCHAR)(pMemBase + PORT0_OFFSET + offset), val);
	}
	else if (PORT1 == port)
	{
		WRITE_REGISTER_UCHAR((PUCHAR)(pMemBase + PORT1_OFFSET + offset), val);
	}
}

static VOID _Send(ULONG_PTR pMemBase, ULONG port, CanMsg_t *pMsg)
{
  UCHAR buf;
  ULONG i;

  buf = (UCHAR) (pMsg->id >> 3);
  _CanRegWrite(pMemBase, SJATXID1, port, buf);
  buf = pMsg->id & 7;
  buf <<= 5;
  if (pMsg->rtr == 1)
  {
    buf |= 0x10;
  }
  buf += pMsg->dlen;
  _CanRegWrite(pMemBase, SJATXID0, port, buf);
  for (i = 0; i<pMsg->dlen; i++)
  {
    _CanRegWrite(pMemBase, SJATXDAT0 + i, port, pMsg->data[i]);
  }

  portSending[port] = TRUE;
  _CanRegWrite(pMemBase, SJACMR, port, sjaCMR_TR);
}

static VOID _IntHandler(WDFDEVICE device, ULONG port)
{
  PDEVICE_CONTEXT devContext = NULL;
  UCHAR irqRegister;
  UCHAR status;
  LONG i;
  UCHAR idBuf[2];
  UCHAR dataBuf[8];
  ULONG_PTR portBase;
  CanMsg_t msg;
  LONG result;

  devContext = PortIOGetDeviceContext(device);
  portBase = (ULONG_PTR)devContext->PortBase;

  //KdPrint(("Port Base=0x%x\n", portBase));

  irqRegister = _CanRegRead(portBase, SJAIR, port);
  status = _CanRegRead(portBase, SJASR, port);

  //KdPrint(("irqRegister=0x%x, status=0x%x\n", irqRegister, status));

  if (irqRegister & sjaIR_RI)
  {
    //KdPrint(("Receive IRQ\n"));

    // Read receive registers
    idBuf[0] = _CanRegRead(portBase, SJARXID1, port);
    idBuf[1] = _CanRegRead(portBase, SJARXID0, port);
    for (i = 0; i < (idBuf[1] & 0x0f); i++)
    {
      dataBuf[i] = _CanRegRead(portBase, SJARXDAT0 + i, port);
    }

    // Parse receice registers
    msg.id = idBuf[0];
    msg.id <<= 3;
    msg.id |= (idBuf[1] >> 5);
    msg.rtr = (idBuf[1] >> 4) & 1;
    msg.dlen = idBuf[1] & 0x0f;
    for (i = 0; i < msg.dlen; i++)
    {
      msg.data[i] = dataBuf[i];
    }

    // Release receive buffer
    _CanRegWrite(portBase, SJACMR, port, sjaCMR_RRB);

    //KdPrint(("msg id:%d, len:%d, data:[%x %x %x %x %x %x %x %x]\n", msg.id, msg.dlen, msg.data[0], msg.data[1], msg.data[2], msg.data[3], msg.data[4], msg.data[5], msg.data[6], msg.data[7]));

    if (TRUE == devContext->DevOpened[port])
    {
      msg.port = (USHORT) port;
      result = CanBufPut(&canFifoRx[port], &msg);
      if (0 != result)
      {
        KdPrint(("_IntHandler CanBufPut Error!\n"));
      }
    }
    else
    {
      KdPrint(("Port %d not opened!", port));
    }
  }

  if (irqRegister & sjaIR_TI)
  {
    //KdPrint(("Transmit IRQ\n"));

    result = CanBufGet(&canFifoTx[port], &msg);
    if (0 == result)
    {
      _Send(portBase, port, &msg);
    }
    else
    {
      // Transmitt FIFO is empty
      portSending[port] = FALSE;
    }
  }
}

NTSTATUS
PortIODeviceCreate(
    PWDFDEVICE_INIT DeviceInit
    )
{
  WDF_OBJECT_ATTRIBUTES           deviceAttributes;
  PDEVICE_CONTEXT                 deviceContext;
  WDF_PNPPOWER_EVENT_CALLBACKS    pnpPowerCallbacks;
  WDFDEVICE                       device;
  NTSTATUS                        status;
  UNICODE_STRING                  ntDeviceName;
  UNICODE_STRING                  win32DeviceName;
  WDF_FILEOBJECT_CONFIG           fileConfig;

	WDF_INTERRUPT_CONFIG            interruptConfig;
	PDEVICE_INTERRUPT_CONTEXT       interruptContext;
   
  PAGED_CODE();
    
  WDF_PNPPOWER_EVENT_CALLBACKS_INIT(&pnpPowerCallbacks);

  //
  // Register pnp/power callbacks so that we can start and stop the timer as 
  // the device gets started and stopped.
  //
  pnpPowerCallbacks.EvtDevicePrepareHardware = PortIOEvtDevicePrepareHardware;
  pnpPowerCallbacks.EvtDeviceReleaseHardware = PortIOEvtDeviceReleaseHardware;
  
  //
  // Register the PnP and power callbacks. Power policy related callbacks will 
  // be registered later in SotwareInit.
  //
  WdfDeviceInitSetPnpPowerEventCallbacks(DeviceInit, &pnpPowerCallbacks);

  WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&deviceAttributes, DEVICE_CONTEXT);    

  WDF_FILEOBJECT_CONFIG_INIT(
                  &fileConfig,
                  WDF_NO_EVENT_CALLBACK, 
                  WDF_NO_EVENT_CALLBACK, 
                  WDF_NO_EVENT_CALLBACK // not interested in Cleanup
                  );
  
  // Let the framework complete create and close
  fileConfig.AutoForwardCleanupClose = WdfFalse;
  
  WdfDeviceInitSetFileObjectConfig(DeviceInit,
                                   &fileConfig,
                                   WDF_NO_OBJECT_ATTRIBUTES);
  
  // TODO Add checking whether there is already device with the same name!
  
  //
  // Create a named deviceobject so that legacy applications can talk to us.
  // Since we are naming the object, we wouldn't able to install multiple
  // instance of this driver. Please note that as per PNP guidelines, we
  // should not name the FDO or create symbolic links. We are doing it because
  // we have a legacy APP that doesn't know how to open an interface.
  //
  RtlInitUnicodeString(&ntDeviceName, PCM3680_DEV_NAME);
  
  status = WdfDeviceInitAssignName(DeviceInit,&ntDeviceName);
  if (!NT_SUCCESS(status)) {
      return status;
  }
  
  WdfDeviceInitSetDeviceType(DeviceInit, PCM3680_TYPE);

  //
  // Call this if the device is not holding a pagefile
  // crashdump file or hibernate file.
  //
  WdfDeviceInitSetPowerPageable(DeviceInit);

  status = WdfDeviceCreate(&DeviceInit, &deviceAttributes, &device);
  if (!NT_SUCCESS(status)) {
      return status;
  }

  //
  // Get the device context and initialize it. WdfObjectGet_DEVICE_CONTEXT is an
  // inline function generated by WDF_DECLARE_CONTEXT_TYPE macro in the
  // device.h header file. This function will do the type checking and return
  // the device context. If you pass a wrong object  handle
  // it will return NULL and assert if run under framework verifier mode.
  //
  deviceContext = PortIOGetDeviceContext(device);

  //
  // This values is based on the hardware design.
  // I'm assuming the address is in I/O space for our hardware.
  // Refer http://support.microsoft.com/default.aspx?scid=kb;en-us;Q323595
  // for more info.
  //        
  deviceContext-> PortMemoryType = 0; 

  //
  // Create a device interface so that application can find and talk
  // to us.
  //
  RtlInitUnicodeString(&win32DeviceName, DOS_DEVICE_NAME);
  
  status = WdfDeviceCreateSymbolicLink(
              device,
              &win32DeviceName);

  if (!NT_SUCCESS(status)) {
      return status;
  }

	// Interrupt
  KdPrint(("Setting up interrupts\n"));

	WDF_INTERRUPT_CONFIG_INIT(&interruptConfig,
		PortIOISR0,
		NULL);

	interruptConfig.EvtInterruptDisable = PortIOEvtInterruptDisable;
	interruptConfig.EvtInterruptEnable = PortIOEvtInterruptEnable;

	WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&deviceAttributes, DEVICE_INTERRUPT_CONTEXT);

	status = WdfInterruptCreate(device,
		&interruptConfig,
		&deviceAttributes,
		&deviceContext->WdfInterrupt[0]);

	if (!NT_SUCCESS(status)) {

		KdPrint(("Couldn't create interrupt 0\n"));
		return status;
	}

	//
	// Interrupt state wait lock...
	//
	WDF_OBJECT_ATTRIBUTES_INIT(&deviceAttributes);
	deviceAttributes.ParentObject = deviceContext->WdfInterrupt[0];

	interruptContext = DeviceGetInterruptContext(deviceContext->WdfInterrupt[0]);

	status = WdfWaitLockCreate(&deviceAttributes,
		&interruptContext->InterruptStateLock
		);

	if (!NT_SUCCESS(status)) {
		KdPrint((" WdfWaitLockCreate for InterruptStateLock failed %!STATUS!\n", status));
		return status;
	}

	//
	// Set interrupt policy
	//
	DeviceSetInterruptPolicy(deviceContext->WdfInterrupt[0]);


  // Setup secod interrupt
  WDF_INTERRUPT_CONFIG_INIT(&interruptConfig,
    PortIOISR1,
    NULL);

  interruptConfig.EvtInterruptDisable = PortIOEvtInterruptDisable;
  interruptConfig.EvtInterruptEnable = PortIOEvtInterruptEnable;

  WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&deviceAttributes, DEVICE_INTERRUPT_CONTEXT);

  status = WdfInterruptCreate(device,
    &interruptConfig,
    &deviceAttributes,
    &deviceContext->WdfInterrupt[1]);

  if (!NT_SUCCESS(status))
  {
    KdPrint(("Couldn't create interrupt 1\n"));
    return status;
  }

  //
  // Interrupt state wait lock...
  //
  WDF_OBJECT_ATTRIBUTES_INIT(&deviceAttributes);
  deviceAttributes.ParentObject = deviceContext->WdfInterrupt[1];

  interruptContext = DeviceGetInterruptContext(deviceContext->WdfInterrupt[1]);

  status = WdfWaitLockCreate(&deviceAttributes,
    &interruptContext->InterruptStateLock
    );

  if (!NT_SUCCESS(status))
  {
    KdPrint((" WdfWaitLockCreate for InterruptStateLock failed %!STATUS!\n", status));
    return status;
  }

  //
  // Set interrupt policy
  //
  DeviceSetInterruptPolicy(deviceContext->WdfInterrupt[1]);
    
  //
  // Initialize the I/O Package and any Queues
  //
  status = PortIOQueueInitialize(device);

  KdPrint(("PortIODeviceCreate done \n"));
  return status;
}


NTSTATUS
PortIOEvtDevicePrepareHardware(
    _In_ WDFDEVICE  Device,    
    _In_ WDFCMRESLIST  ResourcesRaw,    
    _In_ WDFCMRESLIST  ResourcesTranslated    
    )
{
    ULONG  i;
    PCM_PARTIAL_RESOURCE_DESCRIPTOR  desc;
    PCM_PARTIAL_RESOURCE_DESCRIPTOR  descTranslated;
    PDEVICE_CONTEXT deviceContext = NULL;
    NTSTATUS status = STATUS_SUCCESS;
    ULONG irqCount;

    PAGED_CODE();
    
    if ((NULL == ResourcesRaw) || 
        (NULL == ResourcesTranslated)){
        return STATUS_DEVICE_CONFIGURATION_ERROR;        
    }
        
    deviceContext = PortIOGetDeviceContext(Device);
    
    irqCount = 0;

    for (i=0; i < WdfCmResourceListGetCount(ResourcesRaw); i++) {
        
        desc = WdfCmResourceListGetDescriptor(ResourcesRaw, i);
        descTranslated =  WdfCmResourceListGetDescriptor(ResourcesTranslated, i);
        
        switch(desc ->Type)
        {
        case CmResourceTypeMemory:
          deviceContext-> PortBase = (PVOID)
                                  MmMapIoSpace (descTranslated->u.Memory.Start,
                                  descTranslated->u.Memory.Length,
                                  MmNonCached);
          deviceContext-> PortCount = descTranslated->u.Memory.Length;

          deviceContext-> PortWasMapped = TRUE;

          KdPrint(("Mem Resource Translated Memory: (%x) Length: (%d)\n",
                  descTranslated->u.Memory.Start.LowPart,
                  descTranslated->u.Memory.Length));                        
          
          break;

        case CmResourceTypeInterrupt:
          deviceContext->Vector[irqCount] = descTranslated->u.Interrupt.Vector;

          KdPrint(("%d. Interrupt number %d\n", irqCount, deviceContext->Vector[irqCount]));
    
          if (!deviceContext->Vector[irqCount])
          {
            KdPrint(("Bogus vector 0\n"));
            status = STATUS_DEVICE_CONFIGURATION_ERROR;
          }
          
          if (descTranslated->ShareDisposition == CmResourceShareShared)
          {
            KdPrint(("Sharing interrupt with other devices \n"));
          }
          else
          {
            KdPrint(("Interrupt is not shared with other devices\n"));
          }
    
    	  deviceContext->Irql[irqCount] = descTranslated->u.Interrupt.Level;
          deviceContext->Affinity[irqCount] = descTranslated->u.Interrupt.Affinity;
          deviceContext->InterruptMode[irqCount] = Latched;

          irqCount++;
    			break;

        default:
          KdPrint(("Unhandled resource type (0x%x)\n", desc->Type));
          status = STATUS_UNSUCCESSFUL;
          break;
        }

    }

    return status;
}

NTSTATUS
PortIOEvtDeviceReleaseHardware(    
    _In_ WDFDEVICE  Device,    
    _In_ WDFCMRESLIST  ResourcesTranslated )
{
    PDEVICE_CONTEXT deviceContext = NULL;
    ULONG i;

    UNREFERENCED_PARAMETER(ResourcesTranslated);
    
    deviceContext = PortIOGetDeviceContext(Device);

    PAGED_CODE();

    for (i = 0; i < PORT_COUNT; i++)
    {
      if (TRUE == deviceContext->DevOpened[i])
      {
        CanClose(Device, i);
      }
    }
    
    //
    // We received query-remove earlier so free up resources.
    //
    if (deviceContext->PortWasMapped){
        MmUnmapIoSpace(deviceContext->PortBase, deviceContext->PortCount);
        deviceContext->PortWasMapped = FALSE;
    }

    return STATUS_SUCCESS;
}

VOID
DeviceSetInterruptPolicy(
_In_ WDFINTERRUPT WdfInterrupt
)
{
	WDF_INTERRUPT_EXTENDED_POLICY   policyAndGroup;

	WDF_INTERRUPT_EXTENDED_POLICY_INIT(&policyAndGroup);
	policyAndGroup.Priority = WdfIrqPriorityNormal;

	//
	// Set interrupt policy and group preference.
	//
	WdfInterruptSetExtendedPolicy(WdfInterrupt, &policyAndGroup);
}

NTSTATUS
PortIOEvtInterruptEnable(
IN WDFINTERRUPT  Interrupt,
IN WDFDEVICE     AssociatedDevice
)
{
    UNREFERENCED_PARAMETER(Interrupt);
    UNREFERENCED_PARAMETER(AssociatedDevice);

    //KdPrint(("--> SerialEvtInterruptEnable\n"));
  
    //KdPrint(("<-- SerialEvtInterruptEnable\n"));

    return STATUS_SUCCESS;
}

NTSTATUS
PortIOEvtInterruptDisable(
IN WDFINTERRUPT  Interrupt,
IN WDFDEVICE     AssociatedDevice
)
{
	UNREFERENCED_PARAMETER(Interrupt);
	UNREFERENCED_PARAMETER(AssociatedDevice);

	//KdPrint(("--> SerialEvtInterruptDisable\n"));

	//KdPrint(("<-- SerialEvtInterruptDisable\n"));

	return STATUS_SUCCESS;
}


BOOLEAN
PortIOISR0(
IN WDFINTERRUPT Interrupt,
IN ULONG        MessageID
)
{
	UNREFERENCED_PARAMETER(MessageID);

  //KdPrint(("PortIOISR0\n"));
		
  _IntHandler(WdfInterruptGetDevice(Interrupt), PORT0);

	return TRUE;
}


BOOLEAN
PortIOISR1(
IN WDFINTERRUPT Interrupt,
IN ULONG        MessageID
)
{
  UNREFERENCED_PARAMETER(MessageID);

  //KdPrint(("PortIOISR1\n"));

  _IntHandler(WdfInterruptGetDevice(Interrupt), PORT1);

  return TRUE;
}

ULONG CanOpen(WDFDEVICE device, ULONG devNum, ULONG *pPort, ULONG *pHostId, ULONG *pBaud)
{
  PDEVICE_CONTEXT devContext = NULL;
  
  KdPrint(("CANPortOpen devnum: %d\n", devNum));

  if (devNum > PORT1)
  {
    KdPrint(("devNum out of range!\n"));
    return DRV_ERROR;
  }

  devContext = PortIOGetDeviceContext(device);

  *pPort = devNum;
  *pHostId = 0;
  *pBaud = 0;

  if (TRUE == devContext->DevOpened[*pPort])
  {
    KdPrint(("Device already opened!\n"));
    return DRV_ERROR;
  }

  devContext->DevOpened[*pPort] = TRUE;
  KdPrint(("Port %d opened\n", *pPort));
  
  return DRV_SUCCESS;
}

ULONG CanClose(WDFDEVICE device, ULONG port)
{
  PDEVICE_CONTEXT devContext = NULL;
  ULONG_PTR portBase;

  KdPrint(("CANPortClose port: %d\n", port));
  
  devContext = PortIOGetDeviceContext(device);
  portBase = (ULONG_PTR)devContext->PortBase;

  _CanRegWrite(portBase, SJAOCR, port, 0x0);

  _CanRegWrite(portBase,
    SJACR,
    port,
    sjaCR_RR);

  portSending[port] = FALSE;
  devContext->DevOpened[port] = FALSE;

  return DRV_SUCCESS;
}

ULONG CanInit(WDFDEVICE device, ULONG port, ULONG btr0, ULONG btr1, ULONG intMask)
{
  UNREFERENCED_PARAMETER(intMask);

  PDEVICE_CONTEXT devContext = NULL;
  ULONG_PTR portBase;
  ULONG i;

  KdPrint(("CANPortInit port: %d\n", port));

  devContext = PortIOGetDeviceContext(device);
  portBase = (ULONG_PTR)devContext->PortBase;

  if (FALSE == devContext->DevOpened[port])
  {
    KdPrint(("Device not opened!\n"));
    return DRV_ERROR;
  }

  for (i = 0; i < PORT_COUNT; i++)
  {
    CanBufInit(&canFifoTx[i]);
    CanBufInit(&canFifoRx[i]);
  }
  
  CanReset(device, port);
  _CanRegWrite(portBase, SJACR, port, sjaCR_RR);

  CanSetBaud(device, port, btr0, btr1);
  
  return DRV_SUCCESS;
}


ULONG CanReset(WDFDEVICE device, ULONG port)
{
  PDEVICE_CONTEXT devContext = NULL;
  ULONG_PTR portBase;
  UCHAR tmp;
  LARGE_INTEGER li;

  KdPrint(("CANReset port: %d\n", port));

  devContext = PortIOGetDeviceContext(device);
  portBase = (ULONG_PTR)devContext->PortBase;

  devContext = PortIOGetDeviceContext(device);
  
  tmp = _CanRegRead(portBase, SJARESET, port);
  _CanRegWrite(portBase, SJARESET, port, tmp);
  
  li.QuadPart = -10000;    // 1 ms delay
  KeDelayExecutionThread(KernelMode, FALSE, &li);
  
  return DRV_SUCCESS;
}

ULONG CanSetOutCtrl(WDFDEVICE device, ULONG port, ULONG outCtrlCode)
{
  PDEVICE_CONTEXT devContext = NULL;
  ULONG_PTR portBase;

  KdPrint(("CANSetOutCtrl port: %d\n", port));

  devContext = PortIOGetDeviceContext(device);
  portBase = (ULONG_PTR)devContext->PortBase;

  _CanRegWrite(portBase, SJAOCR, port, (UCHAR) outCtrlCode);
  
  return DRV_SUCCESS;
}

ULONG CanSetAcp(WDFDEVICE device, ULONG port, ULONG acpCode, ULONG acpMask)
{
  PDEVICE_CONTEXT devContext = NULL;
  ULONG_PTR portBase;

  KdPrint(("CANSetAcp port: %d\n", port));

  devContext = PortIOGetDeviceContext(device);
  portBase = (ULONG_PTR)devContext->PortBase;

  _CanRegWrite(portBase, SJAACR, port, (UCHAR) acpCode); /* Acceptance code */
  _CanRegWrite(portBase, SJAAMR, port, (UCHAR) acpMask); /* Acceptance mask */

  return DRV_SUCCESS;
}

ULONG CanSetBaud(WDFDEVICE device, ULONG port, ULONG btr0, ULONG btr1)
{
  PDEVICE_CONTEXT devContext = NULL;
  ULONG_PTR portBase;
  UCHAR tmp;

  KdPrint(("CANSetBaud port: %d\n", port));

  devContext = PortIOGetDeviceContext(device);
  portBase = (ULONG_PTR)devContext->PortBase;

  _CanRegWrite(portBase, SJABTR0, port, (UCHAR)btr0); /* BT0 */
  _CanRegWrite(portBase, SJABTR1, port, (UCHAR) btr1); /* BT1 */

  tmp = _CanRegRead(portBase, SJABTR0, port);
  if (tmp != btr0)
  {
    return DRV_ERROR;
  }

  tmp = _CanRegRead(portBase, SJABTR1, port);
  if (tmp != btr1)
  {
    return DRV_ERROR;
  }

  return DRV_SUCCESS;
}

ULONG CanSetNormal(WDFDEVICE device, ULONG port)
{
  PDEVICE_CONTEXT devContext = NULL;
  ULONG_PTR portBase;

  KdPrint(("CANSetNormal port: %d\n", port));

  devContext = PortIOGetDeviceContext(device);
  portBase = (ULONG_PTR)devContext->PortBase;

  _CanRegWrite(portBase,
               SJACR,
               port,
               sjaCR_TIE | sjaCR_EIE | sjaCR_OIE);

  return DRV_SUCCESS;
}

ULONG CanEnableRxInt(WDFDEVICE device, ULONG port)
{

  PDEVICE_CONTEXT devContext = NULL;
  ULONG_PTR portBase;
  UCHAR tmp;

  KdPrint(("CANEnableRxInt port: %d\n", port));

  devContext = PortIOGetDeviceContext(device);
  portBase = (ULONG_PTR)devContext->PortBase;

  tmp = _CanRegRead(portBase, SJACR, port);
  tmp |= sjaCR_RIE;
  _CanRegWrite(portBase,
               SJACR,
               port,
               tmp);
  
  return DRV_SUCCESS;
}

// TODO add result state
ULONG CanSendMsg(WDFDEVICE device, ULONG port, CanMsg_t *pMsg)
{
  PDEVICE_CONTEXT deviceContext;
  LONG result;
  deviceContext = PortIOGetDeviceContext(device);

  //KdPrint(("CANSendMsg port: %d\n", port));

  // Is sending already in process
  if (TRUE == portSending[port])
  {
    // Put message to the FIFO
    result = CanBufPut(&canFifoTx[port], pMsg);
    if (0 != result)
    {
      KdPrint(("CANSendMsg CanBufPut Error!\n"));
      return DRV_ERROR;
    }
  }
  else
  {
    // Start sending  message
    //KdPrint(("CANSendMsg Send\n"));
    _Send((ULONG_PTR) deviceContext->PortBase, port, pMsg);
  }
  
  return DRV_SUCCESS;
}

ULONG CanReceiveMsg(WDFDEVICE device, ULONG port, ULONG maxCount, CanMsg_t *pMsg, ULONG *pNumReturned)
{
  UNREFERENCED_PARAMETER(device);

  ULONG i;
  ULONG limit;
  LONG result;
  ULONG count;

  //KdPrint(("CanReceiveMsg maxcount: %d, port: %d\n", maxCount, port));
    
  if (port > PORT_COUNT - 1)
  {
    KdPrint(("CANReceiveMsg: port %d is out of range!\n", port));
    return DRV_ERROR;
  }
  
  if (CAN_BUF_SIZE < maxCount)
  {
    limit = CAN_BUF_SIZE;
  }
  else
  {
    limit = maxCount;
  }
    
  count = 0;
  for (i = 0; i < limit; i++)
  {
    result = CanBufGet(&canFifoRx[port], &pMsg[i]);
    if (0 != result)
    {
      break;
    }
    count++;
  }

  //KdPrint(("CanReceiveMsg count: %d\n", count));

  *pNumReturned = count;
  
  return DRV_SUCCESS;
}

