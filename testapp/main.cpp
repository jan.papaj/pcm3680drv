﻿#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <string>
#include <cmath>

#include <windows.h>
#include <windowsx.h>
#include <commctrl.h>
#include <mmsystem.h>

#include "CanBus.h"

#define CAN_ITEM_MAX 100
#define CAN_BUF_LEN  1000

HDC dc;
HDC hdc_blt;
HBITMAP bitmap;
HINSTANCE instance;

HWND wnd_main;
HWND statbar;

HWND bt_start;
HWND bt_clear;
HWND bt_usb_boot;
HWND bt_can_boot;
HWND bt_connect;

HWND st_data;
HWND lv_data;

HWND tb_file;

bool app_state = false;

WORD port;

void CommCycle();

typedef struct{
  USHORT       port;
  UCHAR        ff;
  UCHAR        rtr;
  ULONG        id;
  UCHAR        dlen;
  UCHAR        data[8];
  UCHAR        a1;
  UCHAR        a2;
  UCHAR        a3;
} CanMsg_t;


typedef struct
{
  CanMsg_t can_data;
  DWORD count;
  float time_diff;
} can_item_t;

can_item_t can_item[CAN_ITEM_MAX];
DWORD can_item_count = 0;

CanMsg_t can_data_buf_tx[CAN_BUF_LEN];
CanMsg_t can_data_buf_rx[CAN_BUF_LEN];

void CALLBACK TimerFunction(UINT wTimerID, UINT msg, DWORD dwUser, DWORD dw1, DWORD dw2);
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);

UINT m_uResolution;
UINT m_idEvent;

void InitGui()
{
  TCHAR app_name[100] = L"CAN-USB Terminal";
  WNDCLASSEX wc;
  
  // Hlavni okno
  wc.cbSize = sizeof(wc);
  wc.style = CS_HREDRAW | CS_VREDRAW | CS_PARENTDC;
  wc.lpfnWndProc = WndProc;
  wc.cbClsExtra = 0;
  wc.cbWndExtra = 0;
  wc.hInstance = instance;
  wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
  wc.hIconSm = NULL;
  wc.hCursor = (HCURSOR)LoadCursor(NULL, IDC_ARROW);
  wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 11);
  wc.lpszMenuName = NULL;
  wc.lpszClassName = app_name;

  RegisterClassEx(&wc);

  // Main window
  wnd_main = CreateWindowEx(WS_EX_APPWINDOW | WS_EX_WINDOWEDGE, app_name, app_name, WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX, 0, 0, 626, 600, NULL, (HMENU)NULL, instance, NULL);

  statbar = CreateWindowEx(0, STATUSCLASSNAME, L"", WS_CHILD | WS_VISIBLE, 0, 0, 0, 0, wnd_main, (HMENU)NULL, instance, NULL);


  bt_clear = CreateWindowEx(0, WC_BUTTON, L"Vyčistit", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10 + 0, 10, 70, 18, wnd_main, (HMENU)NULL, instance, NULL);
  bt_usb_boot = CreateWindowEx(0, WC_BUTTON, L"USB Flash", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10 + 80, 10, 70, 18, wnd_main, (HMENU)NULL, instance, NULL);
  bt_can_boot = CreateWindowEx(0, WC_BUTTON, L"CAN Flash", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10 + 160, 10, 70, 18, wnd_main, (HMENU)NULL, instance, NULL);
  bt_connect = CreateWindowEx(0, WC_BUTTON, L"Připojit", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 538, 10, 70, 18, wnd_main, (HMENU)NULL, instance, NULL);

  st_data = CreateWindowEx(0, WC_STATIC, L"> Data příjem", WS_CHILD | WS_VISIBLE | SS_LEFT, 10, 35, 100, 18, wnd_main, (HMENU)NULL, instance, NULL);
  lv_data = CreateWindowEx(WS_EX_STATICEDGE, WC_LISTVIEW, L"", WS_CHILD | WS_VISIBLE | LVS_REPORT | WS_BORDER, 10, 55, 600, 480, wnd_main, (HMENU)NULL, instance, NULL);

  //tb_file                = CreateWindowEx( WS_EX_STATICEDGE                   ,WC_LISTVIEW     ,L""                  ,WS_CHILD | WS_VISIBLE | LVS_REPORT | WS_BORDER                                        , 10                           , 55                                   ,600  ,480        ,wnd_main ,( HMENU)NULL  ,instance ,NULL);

  ListView_SetExtendedListViewStyle(lv_data, LVS_EX_GRIDLINES);

  LV_COLUMN lvcol;
  lvcol.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
  lvcol.fmt = LVCFMT_LEFT;

  lvcol.cx = 80;
  lvcol.pszText = TEXT("Počet");
  lvcol.iSubItem = 0;
  ListView_InsertColumn(lv_data, 0, &lvcol);

  lvcol.cx = 110;
  lvcol.pszText = TEXT("Čas příchodu");
  lvcol.iSubItem = 0;
  ListView_InsertColumn(lv_data, 1, &lvcol);

  lvcol.cx = 110;
  lvcol.pszText = TEXT("ID [hex]");
  lvcol.iSubItem = 0;
  ListView_InsertColumn(lv_data, 2, &lvcol);

  lvcol.cx = 50;
  lvcol.pszText = TEXT("Délka");
  lvcol.iSubItem = 0;
  ListView_InsertColumn(lv_data, 3, &lvcol);

  lvcol.cx = 180;
  lvcol.pszText = TEXT("Data [0, 1, 2, 3, 4, 5, 6, 7]");
  lvcol.iSubItem = 0;
  ListView_InsertColumn(lv_data, 4, &lvcol);

  HFONT font = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

  SendMessage(bt_start, WM_SETFONT, (WPARAM)font, (LPARAM)TRUE);
  SendMessage(bt_clear, WM_SETFONT, (WPARAM)font, (LPARAM)TRUE);
  SendMessage(bt_usb_boot, WM_SETFONT, (WPARAM)font, (LPARAM)TRUE);
  SendMessage(bt_can_boot, WM_SETFONT, (WPARAM)font, (LPARAM)TRUE);
  SendMessage(bt_connect, WM_SETFONT, (WPARAM)font, (LPARAM)TRUE);
  SendMessage(st_data, WM_SETFONT, (WPARAM)font, (LPARAM)TRUE);
  SendMessage(lv_data, WM_SETFONT, (WPARAM)font, (LPARAM)TRUE);

  ShowWindow(wnd_main, 1);
  UpdateWindow(wnd_main);

  SetTimer(wnd_main, 0, 50, NULL);
  SendMessage(wnd_main, WM_TIMER, 0, 0);
}

int WINAPI WinMain(HINSTANCE instance, HINSTANCE prev_instance, PSTR cmd_line, int cmd_show)
{
  MSG msg;
  WORD hostid, prebaud;

  CANPortOpen(0, &port, &hostid, &prebaud);
  CANInit(port, 0x00, 0x1c, 0);
//CANInit(port, 0x43, 0x23, 0);
  CANReset(port);
  CANSetOutCtrl(port, 0xfa);
  CANSetAcp(port, 0, 0xff);
  CANSetBaud(port, 0x00, 0x1c);
//CANSetBaud(port, 0x43, 0x23);
  CANSetNormal(port);
  CANEnableRxInt(port);
  
  // GUI init
  InitGui();

  TIMECAPS tc;
  UINT period = 10;
  timeGetDevCaps(&tc, sizeof(TIMECAPS));
  m_uResolution = min(max(tc.wPeriodMin, 0), tc.wPeriodMax);
  timeBeginPeriod(period);

  // create the timer

  m_idEvent = timeSetEvent(
    period,
    m_uResolution,
    TimerFunction,
    0,
    TIME_PERIODIC);

  while (GetMessage(&msg, NULL, 0, 0))
  {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }
  return 0;
}

void CALLBACK TimerFunction(UINT wTimerID, UINT msg,
  DWORD dwUser, DWORD dw1, DWORD dw2)
{
  CommCycle();
}

void CommCycle()
{
  wchar_t wstr[100];
  LV_ITEM lvi;
  int result;
  WORD count;
  BOOL ready;

  bool found;

  count = 0;
  result = CANReadFile(port, CAN_BUF_LEN, &can_data_buf_rx[0], &count);

  wchar_t str[100];
  wsprintf(str, L"count:%d result:%d", count, result);
  //MessageBox(NULL, str, L"", 0);
  for (ULONG j = 0; j < count; j++)
  {
    // zkusit najit polozku v listu
    found = false;
    for (int i = 0; i < can_item_count; i++)
    {
      if (can_item[i].can_data.id == can_data_buf_rx[j].id)
      {
        can_item[i].count++;
        can_item[i].time_diff = 0;

        can_item[i].can_data.dlen = can_data_buf_rx[j].dlen;
        memcpy(&can_item[i].can_data.data[0], &can_data_buf_rx[j].data[0], can_data_buf_rx[j].dlen);
        
        found = true;
      }
    }

    // pokud polozka nebyla nalezena, tak pridat novou
    if (!found && can_item_count < CAN_ITEM_MAX)
    {
      can_item[can_item_count].count = 1;
      can_item[can_item_count].time_diff = 0;

      can_item[can_item_count].can_data.id = can_data_buf_rx[j].id;
      can_item[can_item_count].can_data.dlen = can_data_buf_rx[j].dlen;
      memcpy(&can_item[can_item_count].can_data.data[0], &can_data_buf_rx[j].data[0], can_data_buf_rx[j].dlen);

      ZeroMemory(&lvi, sizeof(LV_ITEM));

      // pocet
      wsprintf(wstr, L"1");
      lvi.mask = LVIF_TEXT;
      lvi.iItem = can_item_count;
      lvi.pszText = wstr;
      ListView_InsertItem(lv_data, &lvi);

      // cas prichodu
      wsprintf(wstr, L"%d", can_item[can_item_count].time_diff);
      ListView_SetItemText(lv_data, can_item_count, 1, wstr);

      // id
      wsprintf(wstr, L"%03X", can_item[can_item_count].can_data.id);
      ListView_SetItemText(lv_data, can_item_count, 2, wstr);

      // len
      wsprintf(wstr, L"%d", can_item[can_item_count].can_data.dlen);
      ListView_SetItemText(lv_data, can_item_count, 3, wstr);

      // data
      wsprintf(wstr, L"%02d %02d %02d %02d %02d %02d %02d %02d",
        can_item[can_item_count].can_data.data[0],
        can_item[can_item_count].can_data.data[1],
        can_item[can_item_count].can_data.data[2],
        can_item[can_item_count].can_data.data[3],
        can_item[can_item_count].can_data.data[4],
        can_item[can_item_count].can_data.data[5],
        can_item[can_item_count].can_data.data[6],
        can_item[can_item_count].can_data.data[7]);
      ListView_SetItemText(lv_data, can_item_count, 4, wstr);

      can_item_count++;
    }
  }

  for (int i = 0; i < 3; i++)
  {
    memset(&can_data_buf_tx[i], 0, sizeof(CanMsg_t));

    can_data_buf_tx[i].id = 0x100 + i;
    can_data_buf_tx[i].dlen = 8;
    can_data_buf_tx[i].rtr = 0;
    can_data_buf_tx[i].data[0] = i*10;
    can_data_buf_tx[i].data[1] = i*10;
    can_data_buf_tx[i].data[2] = i*10;
    can_data_buf_tx[i].data[3] = i*10;
    can_data_buf_tx[i].data[4] = i*10;
    can_data_buf_tx[i].data[5] = i*10;
    can_data_buf_tx[i].data[6] = i*10;
    can_data_buf_tx[i].data[7] = i*10;

    CANWriteFile(port, &ready, &can_data_buf_tx[i]);
  }
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
  wchar_t wstr[100];
  char str[100];

  switch (message)
  {
  case WM_CLOSE:
    // destroy the timer
    timeKillEvent(m_idEvent);

    // reset the timer
    timeEndPeriod(m_uResolution);

    Sleep(100);

    KillTimer(wnd_main, 0);
    
    CANPortClose(port);
    Sleep(100);

    PostQuitMessage(0);
    break;

  case WM_TIMER:
    for (ULONG i = 0; i < can_item_count; i++)
    {
      // pocet
      wsprintf(wstr, L"%d", can_item[i].count);
      ListView_SetItemText(lv_data, i, 0, wstr);

      // cas prichodu
      sprintf_s(str, "%.4f", can_item[i].time_diff);
      mbstowcs(wstr, str, sizeof(str));
      ListView_SetItemText(lv_data, i, 1, wstr);

      // id
      wsprintf(wstr, L"%03X", can_item[i].can_data.id);
      ListView_SetItemText(lv_data, i, 2, wstr);

      // len
      wsprintf(wstr, L"%d", can_item[i].can_data.dlen);
      ListView_SetItemText(lv_data, i, 3, wstr);

      // data
      wsprintf(wstr, L"%02d %02d %02d %02d %02d %02d %02d %02d",
        can_item[i].can_data.data[0],
        can_item[i].can_data.data[1],
        can_item[i].can_data.data[2],
        can_item[i].can_data.data[3],
        can_item[i].can_data.data[4],
        can_item[i].can_data.data[5],
        can_item[i].can_data.data[6],
        can_item[i].can_data.data[7]);
      ListView_SetItemText(lv_data, i, 4, wstr);
    }
    break;

  case WM_PAINT:
    break;

  case WM_COMMAND:
    break;
  }
  return DefWindowProc(hwnd, message, wparam, lparam);
}