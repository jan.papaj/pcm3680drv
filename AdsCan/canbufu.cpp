#include "stdafx.h"

VOID CanBufInit(CanBuf_t *pBuf)
{
  memset( pBuf, 0, sizeof( CanBuf_t));
  pBuf->head = 0;
  pBuf->tail = 0;
  pBuf->size = 0;
}

LONG CanBufPut( CanBuf_t *pBuf, CanMsg_t *pMsg)
{
  LONG ret;
  
  ret = -1;
  
  // Check for ring buffer overflow
  if ( ( pBuf->tail + 1) % CAN_BUF_SIZE != pBuf->head)
  {
    // Add data to ring buffer
    memcpy( &pBuf->msg[pBuf->tail], pMsg, sizeof( CanMsg_t));
    pBuf->tail++;
    pBuf->size++;
    if( pBuf->tail >= CAN_BUF_SIZE)
    {
      pBuf->tail = 0;
    }
    
    ret = 0;
  }
  
  return ret;
}

LONG CanBufGet( CanBuf_t *pBuf, CanMsg_t *pMsg)
{
  LONG ret;
  
  ret = -1;
  
  if( pBuf->head != pBuf->tail)
  {
    memcpy( pMsg, &pBuf->msg[pBuf->head], sizeof( CanMsg_t));

    pBuf->head++;
    pBuf->size--;
    if( pBuf->head >= CAN_BUF_SIZE)
    {
      pBuf->head = 0;
    }
    ret = 0;
  }  
  return ret;
}
