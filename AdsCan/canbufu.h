#ifndef _CANBUFU_H_
#define _CANBUFU_H_

#include <windows.h>
#include "canmsg.h"

#define CAN_BUF_SIZE 2048

typedef struct
{
  CanMsg_t msg[CAN_BUF_SIZE];
  unsigned long head;
  unsigned long tail;
  unsigned long size;
} CanBuf_t;

void CanBufInit( CanBuf_t *pBuf);
long CanBufPut( CanBuf_t *pBuf, CanMsg_t *pMsg);
long CanBufGet( CanBuf_t *pBuf, CanMsg_t *pMsg);

#endif
