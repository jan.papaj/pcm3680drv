// AdsCan.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#define PORT_COUNT    2

#define PORT0         0
#define PORT1         1

#define PORT0_OFFSET  0x0
#define PORT1_OFFSET  0x200

static HANDLE  hndFile;
static BOOLEAN opened;
static CanBuf_t canFifoRx[PORT_COUNT];
static CanMsg_t rxBuf[CAN_BUF_SIZE * 2];

static LONG _Open();
static LONG _Close();
static LONG _SendIOCTL(LONG ioctlCode, PGENPORT_IN pIn, PGENPORT_OUT pOut);

static LONG _Open()
{
  int i;
  
  for (i = 0; i < PORT_COUNT; i++)
  {
    CanBufInit(&canFifoRx[i]);
  }

  hndFile = CreateFile("\\\\.\\pcm3680",                    // Open the Device "file"
    GENERIC_READ | GENERIC_WRITE,
    FILE_SHARE_READ | FILE_SHARE_WRITE,
    NULL,
    OPEN_EXISTING,
    0,
    NULL
    );

  if (hndFile == INVALID_HANDLE_VALUE)        // Was the device opened?
  {
    printf("Unable to open the device.\n");
    return -1;
  }

  return 0;
}

static LONG _Close()
{
  if (!CloseHandle(hndFile))                  // Close the Device "file".
  {
    printf("Failed to close device.\n");
    return -1;
  }

  return 0;
}

static LONG _SendIOCTL(LONG ioctlCode, PGENPORT_IN pIn, PGENPORT_OUT pOut)
{
  LONG ret;
  // The following is returned by IOCTL.  It is true if the read succeeds.
  BOOL    IoctlResult;

  // The following parameters are used in the IOCTL call
  // Handle to device, obtain from CreateFile
  ULONG   DataLength;
  DWORD   ReturnedLength;     // Number of bytes returned

  GENPORT_OUT tmpOut;


  ret = 0;
  DataLength = sizeof(GENPORT_OUT);

  IoctlResult = DeviceIoControl(hndFile,            // Handle to device
    ioctlCode,          // IO Control code for Read
    pIn,                // Buffer to driver.
    sizeof(GENPORT_IN), // Length of buffer in bytes.
    &tmpOut,            // Buffer from driver.
    sizeof(GENPORT_OUT),// Length of buffer in bytes.
    &ReturnedLength,    // Bytes placed in DataBuffer.
    NULL                // NULL means wait till op. completes.
    );

  if (IoctlResult)                            // Did the IOCTL succeed?
  {
    if (ReturnedLength != DataLength)
    {
      printf(
        "Ioctl transfered %d bytes, expected %d bytes\n",
        ReturnedLength, DataLength);
      ret = -1;
    }
    else
    {
      memcpy(pOut, &tmpOut, sizeof(GENPORT_OUT));
    }
  }
  else
  {
    printf("Ioctl failed with code %ld\n", GetLastError());
    ret = -1;
  }

  return ret;
}

FEXPORT ULONG CANPortOpen(WORD devNum, WORD *pPort, WORD *pHostId, WORD *pBaud)
{
  GENPORT_IN in;
  GENPORT_OUT out;
  LONG result;
  WORD ret;

  if (devNum >= PORT_COUNT)
  {
    return CommConfigFailed;
  }

  ret = SUCCESS;

  result = _Open();
  if (0 == result)
  {
    opened = TRUE;
    in.u.InOpen.devNum = devNum;

    result = _SendIOCTL(IOCTL_CAN_OPEN, &in, &out);
    if (0 != result)
    {
      ret = CommConfigFailed;
    }
    else
    {
      if (DRV_SUCCESS == out.state)
      {
        *pPort = (WORD) out.u.OutOpen.port;
        *pHostId = (WORD) out.u.OutOpen.hostId;
        *pBaud = (WORD) out.u.OutOpen.preBaud;
        ret = SUCCESS;
      }
      else
      {
        ret = CommConfigFailed;
      }
    }
  }
  else
  {
    ret = CommConfigFailed;
  }

  return ret;
}

FEXPORT ULONG CANPortClose(WORD port)
{
  GENPORT_IN in;
  GENPORT_OUT out;
  LONG result;
  ULONG ret;

  if (port >= PORT_COUNT)
  {
    return CommConfigFailed;
  }

  ret = SUCCESS;

  in.u.InClose.port = port;

  result = _SendIOCTL(IOCTL_CAN_CLOSE, &in, &out);
  if (0 != result)
  {
    ret = CommConfigFailed;
  }
  else
  {
    if (DRV_SUCCESS == out.state)
    {
      ret = SUCCESS;
    }
    else
    {
      ret = CommConfigFailed;
    }
  }

  if (TRUE == opened)
  {
    _Close();
  }

  return ret;
}

FEXPORT ULONG CANInit(WORD port, WORD btr0, WORD btr1, UCHAR intMask)
{
  GENPORT_IN in;
  GENPORT_OUT out;
  LONG result;
  ULONG ret;

  if (port >= PORT_COUNT)
  {
    return CommConfigFailed;
  }

  ret = SUCCESS;

  in.u.InInit.port = port;
  in.u.InInit.btr0 = btr0;
  in.u.InInit.btr1 = btr1;
  in.u.InInit.intMask = intMask;

  result = _SendIOCTL(IOCTL_CAN_INIT, &in, &out);
  if (0 != result)
  {
    ret = CommConfigFailed;
  }
  else
  {
    if (DRV_SUCCESS == out.state)
    {
      ret = SUCCESS;
    }
    else
    {
      ret = CommConfigFailed;
    }
  }

  return ret;
}

FEXPORT ULONG CANReset(WORD port)
{
  GENPORT_IN in;
  GENPORT_OUT out;
  LONG result;
  ULONG ret;

  if (port >= PORT_COUNT)
  {
    return CommConfigFailed;
  }

  ret = SUCCESS;

  in.u.InReset.port = port;

  result = _SendIOCTL(IOCTL_CAN_RESET, &in, &out);
  if (0 != result)
  {
    ret = CommConfigFailed;
  }
  else
  {
    if (DRV_SUCCESS == out.state)
    {
      ret = SUCCESS;
    }
    else
    {
      ret = CommConfigFailed;
    }
  }

  return ret;
}

FEXPORT ULONG CANSetOutCtrl(WORD port, WORD outCtrlCode)
{
  GENPORT_IN in;
  GENPORT_OUT out;
  LONG result;
  ULONG ret;

  if (port >= PORT_COUNT)
  {
    return CommConfigFailed;
  }

  ret = SUCCESS;

  in.u.InSetOutCtrl.port = port;
  in.u.InSetOutCtrl.outCtrlCode = outCtrlCode;

  result = _SendIOCTL(IOCTL_CAN_SET_OUT_CTRL, &in, &out);
  if (0 != result)
  {
    ret = CommConfigFailed;
  }
  else
  {
    if (DRV_SUCCESS == out.state)
    {
      ret = SUCCESS;
    }
    else
    {
      ret = CommConfigFailed;
    }
  }

  return ret;
}

FEXPORT ULONG CANSetAcp(WORD port, WORD acpCode, WORD acpMask)
{
  GENPORT_IN in;
  GENPORT_OUT out;
  LONG result;
  ULONG ret;

  if (port >= PORT_COUNT)
  {
    return CommConfigFailed;
  }

  ret = SUCCESS;

  in.u.InSetAcp.port = port;
  in.u.InSetAcp.acpCode = acpCode;
  in.u.InSetAcp.acpMask = acpMask;

  result = _SendIOCTL(IOCTL_CAN_SET_ACP, &in, &out);
  if (0 != result)
  {
    ret = CommConfigFailed;
  }
  else
  {
    if (DRV_SUCCESS == out.state)
    {
      ret = SUCCESS;
    }
    else
    {
      ret = CommConfigFailed;
    }
  }

  ret = (WORD) out.state;

  return ret;
}

FEXPORT ULONG CANSetBaud(WORD port, WORD btr0, WORD btr1)
{
  GENPORT_IN in;
  GENPORT_OUT out;
  LONG result;
  ULONG ret;

  if (port >= PORT_COUNT)
  {
    return CommConfigFailed;
  }

  ret = SUCCESS;

  in.u.InSetBaud.port = port;
  in.u.InSetBaud.btr0 = btr0;
  in.u.InSetBaud.btr1 = btr1;

  result = _SendIOCTL(IOCTL_CAN_SET_BAUD, &in, &out);
  if (0 != result)
  {
    ret = CommConfigFailed;
  }
  else
  {
    if (DRV_SUCCESS == out.state)
    {
      ret = SUCCESS;
    }
    else
    {
      ret = CommConfigFailed;
    }
  }

  return ret;
}

FEXPORT ULONG CANSetNormal(WORD port)
{
  GENPORT_IN in;
  GENPORT_OUT out;
  LONG result;
  ULONG ret;

  if (port >= PORT_COUNT)
  {
    return CommConfigFailed;
  }

  ret = SUCCESS;

  in.u.InSetNormal.port = port;

  result = _SendIOCTL(IOCTL_CAN_SET_NORMAL, &in, &out);
  if (0 != result)
  {
    ret = CommConfigFailed;
  }
  else
  {
    if (DRV_SUCCESS == out.state)
    {
      ret = SUCCESS;
    }
    else
    {
      ret = CommConfigFailed;
    }
  }

  return ret;
}

FEXPORT ULONG CANEnableRxInt(WORD port)
{
  GENPORT_IN in;
  GENPORT_OUT out;
  LONG result;
  ULONG ret;

  if (port >= PORT_COUNT)
  {
    return CommConfigFailed;
  }

  ret = SUCCESS;

  in.u.InEnableRxInt.port = port;

  result = _SendIOCTL(IOCTL_CAN_ENABLE_RX_INT, &in, &out);
  if (0 != result)
  {
    ret = CommConfigFailed;
  }
  else
  {
    if (DRV_SUCCESS == out.state)
    {
      ret = SUCCESS;
    }
    else
    {
      ret = CommConfigFailed;
    }
  }

  return ret;
}

FEXPORT ULONG CANWriteFile(WORD port, BOOL *pReady, PVOID pMsg)
{
  BOOL result;
  ULONG ret;
  DWORD numberOfBytesWritten;

  if (port >= PORT_COUNT)
  {
    return CommTransmitFailed;
  }

  ret = SUCCESS;

  *pReady = FALSE;
  ((CanMsg_t*)pMsg)->port = port;

  result = WriteFile(hndFile, pMsg, sizeof(CanMsg_t), &numberOfBytesWritten, 0);
  
  if (FALSE == result)
  {
    ret = CommTransmitFailed;
  }
  else
  {
    if (numberOfBytesWritten == sizeof(CanMsg_t))
    {
      *pReady = TRUE;
    }
    if (numberOfBytesWritten == 0)
    {
      *pReady = FALSE;
    }
    else
    {
      ret = CommTransmitFailed;
    }
  }

  return ret;
}

FEXPORT ULONG CANReadFile(WORD port, WORD maxCount, PVOID pMsg, WORD *pNumReturned)
{
  BOOL result;
  ULONG ret;
  DWORD bytesRead;
  WORD copied;
  CanMsg_t* pDst;
  LONG lresult;

  if (port >= PORT_COUNT)
  {
    return CommReadFailed;
  }

  copied = 0;
  *pNumReturned = 0;
  pDst = (CanMsg_t*)pMsg;
  ret = SUCCESS;

  if ( 0 < maxCount)
  {
    // At first read appropriate ring buffer for messages
    lresult = CanBufGet(&canFifoRx[port], &pDst[copied]);
    while (lresult == 0 &&
           copied < maxCount)
    {
      copied++;
      lresult = CanBufGet(&canFifoRx[port], &pDst[copied]);
    }

    if (copied < maxCount)
    {
      // Try to read messages from driver
      result = ReadFile(hndFile, &rxBuf, CAN_BUF_SIZE * 2 * sizeof(CanMsg_t), &bytesRead, 0);

      if (FALSE == result)
      {
        ret = CommReceiveFailed;
      }
      else
      {
        if (0 == bytesRead % sizeof(CanMsg_t))
        {
          ULONG messagesCount = bytesRead / sizeof(CanMsg_t);
          for (ULONG i = 0; i < messagesCount; i++)
          {
            // Message to be returned
            if (rxBuf[i].port == port &&
                copied < maxCount)
            {
              memcpy(&pDst[copied], &rxBuf[i], sizeof(CanMsg_t));
              copied++;
            }
            // Message to be stored
            else
            {
              if (rxBuf[i].port < PORT_COUNT)
              {
                CanBufPut(&canFifoRx[rxBuf[i].port], &rxBuf[i]);
              }
            }
          }
        }
      }
    }
    *pNumReturned = copied;
  }

  return ret;
}
