#ifndef _CANBUS_H_
#define _CANBUS_H_

#include <windows.h>

#ifdef ADSCAN_EXPORTS
  #define FEXPORT extern __declspec(dllexport)
#else
  #define FEXPORT extern __declspec(dllimport)
#endif

/**************************************************************************
Define Status Code
***************************************************************************/
#define SUCCESS                  0
#define DrvErrorCode             1
#define KeErrorCode              100
#define DnetErrorCode            200
#define GeniDrvErrorCode         300
#define OPCErrorCode             1000
#define MemoryAllocateFailed     (DrvErrorCode + 0)
#define ConfigDataLost           (DrvErrorCode + 1)
#define InvalidDeviceHandle      (DrvErrorCode + 2)
#define AIConversionFailed       (DrvErrorCode + 3)
#define AIScaleFailed            (DrvErrorCode + 4)
#define SectionNotSupported      (DrvErrorCode + 5)
#define InvalidChannel           (DrvErrorCode + 6)
#define InvalidGain              (DrvErrorCode + 7)
#define DataNotReady             (DrvErrorCode + 8)
#define InvalidInputParam        (DrvErrorCode + 9)
#define NoExpansionBoardConfig   (DrvErrorCode + 10)
#define InvalidAnalogOutValue    (DrvErrorCode + 11)
#define ConfigIoPortFailed       (DrvErrorCode + 12)
#define CommOpenFailed           (DrvErrorCode + 13)
#define CommTransmitFailed       (DrvErrorCode + 14)
#define CommReadFailed           (DrvErrorCode + 15)
#define CommReceiveFailed        (DrvErrorCode + 16)
#define CommConfigFailed         (DrvErrorCode + 17)
#define CommChecksumError        (DrvErrorCode + 18)
#define InitError                (DrvErrorCode + 19)
#define DMABufAllocFailed        (DrvErrorCode + 20)
#define IllegalSpeed             (DrvErrorCode + 21)
#define ChanConflict             (DrvErrorCode + 22)
#define BoardIDNotSupported      (DrvErrorCode + 23)
#define FreqMeasurementFailed    (DrvErrorCode + 24)
#define CreateFileFailed         (DrvErrorCode + 25)
#define FunctionNotSupported     (DrvErrorCode + 26)
#define LoadLibraryFailed        (DrvErrorCode + 27)
#define GetProcAddressFailed     (DrvErrorCode + 28)
#define InvalidDriverHandle      (DrvErrorCode + 29)
#define InvalidModuleType        (DrvErrorCode + 30)
#define InvalidInputRange        (DrvErrorCode + 31)
#define InvalidWindowsHandle     (DrvErrorCode + 32)
#define InvalidCountNumber       (DrvErrorCode + 33)
#define InvalidInterruptCount    (DrvErrorCode + 34)
#define InvalidEventCount        (DrvErrorCode + 35)
#define OpenEventFailed          (DrvErrorCode + 36)
#define InterruptProcessFailed   (DrvErrorCode + 37)
#define InvalidDOSetting         (DrvErrorCode + 38)
#define InvalidEventType         (DrvErrorCode + 39)
#define EventTimeOut             (DrvErrorCode + 40)
#define InvalidDmaChannel        (DrvErrorCode + 41)
#define IntDamChannelBusy        (DrvErrorCode + 42)
//
#define CheckRunTimeClassFailed  (DrvErrorCode + 43)
#define CreateDllLibFailed       (DrvErrorCode + 44)
#define ExceptionError           (DrvErrorCode + 45)
#define RemoveDeviceFailed       (DrvErrorCode + 46)
#define BuildDeviceListFailed    (DrvErrorCode + 47)
#define NoIOFunctionSupport      (DrvErrorCode + 48)

//\\\\\\\\\\\\\\\\\\\ V2.0B /////////////////////
#define ResourceConflict        (DrvErrorCode + 49)
///////////////////// V2.0B \\\\\\\\\\\\\\\\\\\\\/

//\\\\\\\\\\\\\\\\\\\ V2.1 //////////////////////
#define InvalidClockSource		 (DrvErrorCode + 50)
#define InvalidPacerRate       (DrvErrorCode + 51)
#define InvalidTriggerMode     (DrvErrorCode + 52)
#define InvalidTriggerEdge     (DrvErrorCode + 53)
#define InvalidTriggerSource   (DrvErrorCode + 54)
#define InvalidTriggerVoltage  (DrvErrorCode + 55)
#define InvalidCyclicMode      (DrvErrorCode + 56)
#define InvalidDelayCount      (DrvErrorCode + 57)
#define InvalidBuffer          (DrvErrorCode + 58)
#define OverloadedPCIBus       (DrvErrorCode + 59)
#define OverloadedInterruptRequest (DrvErrorCode + 60)
///////////////////// V2.1 \\\\\\\\\\\\\\\\\\\\\\/

//\\\\\\\\\\\\\\\\\\\ V2.0C /////////////////////
#define ParamNameNotSupported      (DrvErrorCode + 61)
///////////////////// V2.0C /////////////////////

/*#define NoSerialFunctionSupported   (DrvErrorCode + 49)
#define NoAiFunctionSupported      (DrvErrorCode + 50
#define NoAoFunctionSupported      (DrvErrorCode + 51)
#define NoDiFunctionSupported      (DrvErrorCode + 52)
#define NoDoFunctionSupported      (DrvErrorCode + 53)
#define NoAlarmFunctionSupported    (DrvErrorCode + 54)
#define NoCounterFunctionSupported  (DrvErrorCode + 55)
#define NoTempFunctionSupported      (DrvErrorCode + 56)
#define NoIOFunctionSupported      (DrvErrorCode + 57)*/
#define NoSerialFunctionSupported  (GeniDrvErrorCode + 5)
#define NoAiFunctionSupported      (GeniDrvErrorCode + 6)
#define NoAoFunctionSupported      (GeniDrvErrorCode + 7)
#define NoDiFunctionSupported      (GeniDrvErrorCode + 8)
#define NoDoFunctionSupported      (GeniDrvErrorCode + 9)
#define NoAlarmFunctionSupported   (GeniDrvErrorCode + 10)
#define NoCounterFunctionSupported (GeniDrvErrorCode + 11)
#define NoTempFunctionSupported    (GeniDrvErrorCode + 12)
#define NoIOFunctionSupported      (GeniDrvErrorCode + 13)

#define KeInvalidHandleValue     (KeErrorCode + 0)
#define KeFileNotFound           (KeErrorCode + 1)
#define KeInvalidHandle          (KeErrorCode + 2)
#define KeTooManyCmds            (KeErrorCode + 3)
#define KeInvalidParameter       (KeErrorCode + 4)
#define KeNoAccess               (KeErrorCode + 5)
#define KeUnsuccessful           (KeErrorCode + 6)
#define KeConInterruptFailure    (KeErrorCode + 7)
#define KeCreateNoteFailure      (KeErrorCode + 8)
#define KeInsufficientResources  (KeErrorCode + 9)
#define KeHalGetAdapterFailure   (KeErrorCode +10)
#define KeOpenEventFailure       (KeErrorCode +11)
#define KeAllocCommBufFailure    (KeErrorCode +12)
#define KeAllocMdlFailure        (KeErrorCode +13)
#define KeBufferSizeTooSmall     (KeErrorCode +14)

#define DNInitFailed              (DnetErrorCode + 1)
#define DNSendMsgFailed           (DnetErrorCode + 2)
#define DNRunOutOfMsgID           (DnetErrorCode + 3)
#define DNInvalidInputParam       (DnetErrorCode + 4)
#define DNErrorResponse           (DnetErrorCode + 5)
#define DNNoResponse              (DnetErrorCode + 6)
#define DNBusyOnNetwork           (DnetErrorCode + 7)
#define DNUnknownResponse         (DnetErrorCode + 8)
#define DNNotEnoughBuffer         (DnetErrorCode + 9)
#define DNFragResponseError       (DnetErrorCode + 10)
#define DNTooMuchDataAck          (DnetErrorCode + 11)
#define DNFragRequestError        (DnetErrorCode + 12)
#define DNEnableEventError        (DnetErrorCode + 13)
#define DNCreateOrOpenEventError  (DnetErrorCode + 14)
#define DNIORequestError          (DnetErrorCode + 15)
#define DNGetEventNameError       (DnetErrorCode + 16)
#define DNTimeOutError            (DnetErrorCode + 17)
#define DNOpenFailed              (DnetErrorCode + 18)
#define DNCloseFailed             (DnetErrorCode + 19)
#define DNResetFailed             (DnetErrorCode + 20)


/**************************************************************************
Can Bus operation result Code Table
***************************************************************************/
#define CanErrorCode                2000
#define CANPortIsNotOpened          (CanErrorCode + 0)
#define CANInvalidFrameFormat       (CanErrorCode + 1)
#define CANInvalidPortIndex         (CanErrorCode + 2)
#define CANIncompatibleProtocol     (CanErrorCode + 3)
#define CANControllerInResetMode    (CanErrorCode + 4)
#define CANControllerInWorkMode     (CanErrorCode + 5)
#define CANInvalidDataLength        (CanErrorCode + 6)
#define CANBusError                 (CanErrorCode + 7)
#define CANTimeOut                  (CanErrorCode + 8)
#define CANInvalidParameter         (CanErrorCode + 9)
#define CANDataNotReady             (CanErrorCode + 10)
#define CANBusClosed                (CanErrorCode + 11)
#define CANOpenEventFailed          (CanErrorCode + 12)
#define CANCreateDeviceFailed       (CanErrorCode + 13)
#define CANPortOpenedAlready        (CanErrorCode + 14)
#define CANQueryRegistryFailed      (CanErrorCode + 15)
#define CANMapMemoryFailed          (CanErrorCode + 16)
#define CANPortIsNotInitialized     (CanErrorCode + 17)
#define CANPortInitializedAlready   (CanErrorCode + 18)
#define CANSetupPortFailed          (CanErrorCode + 19)


FEXPORT ULONG CANPortOpen(WORD devNum, WORD *pPort, WORD *pHostId, WORD *pBaud);
FEXPORT ULONG CANPortClose(WORD port);
FEXPORT ULONG CANInit(WORD port, WORD btr0, WORD btr1, UCHAR intMask);
FEXPORT ULONG CANReset(WORD port);
FEXPORT ULONG CANSetOutCtrl(WORD port, WORD outCtrlCode);
FEXPORT ULONG CANSetAcp(WORD port, WORD acpCode, WORD acpMask);
FEXPORT ULONG CANSetBaud(WORD port, WORD btr0, WORD btr1);
FEXPORT ULONG CANSetNormal(WORD port);
FEXPORT ULONG CANEnableRxInt(WORD port);
FEXPORT ULONG CANWriteFile(WORD port, BOOL *pReady, PVOID pMsg);
FEXPORT ULONG CANReadFile (WORD port, WORD maxCount, PVOID pMsg, WORD *pNumReturned);

#endif